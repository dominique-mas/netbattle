package netbattle.entities;

import java.util.HashMap;
import java.util.Random;

/**
 * This class represents the enemy. An enemy has boats on the grid. The player
 * tries to sink the enemy's boats.
 * 
 * @author Dominique
 *
 */
public class Enemy {

	/**
	 * Position of the enemy.
	 */
	private HashMap<Integer, Boolean> position;
	/**
	 * True if the boat is sunk.
	 */
	private boolean sunk;

	/**
	 * Returns enemy's position.
	 * 
	 * @return the enemy's position, as an HashMap object.
	 */
	public HashMap<Integer, Boolean> getPosition() {
		return position;
	}

	public void setPosition(HashMap<Integer, Boolean> position) {
		this.position = position;
	}

	/**
	 * Default constructor. Il n'y avait pas vraiment besoin de mettre un
	 * commentaire, mais c'est pour le fun !!!
	 */
	public Enemy() {
		position = new HashMap<Integer, Boolean>();
		sunk = false;
	}

	public void initPosition(int length, int gridLength) {
		Random r = new Random();
		int start = r.nextInt(gridLength - length + 1);
		// position = new int[length];
		for (int i = 0; i < 3; i++) {
			// position[i] = start + i;
			position.put(start + i, false);
		}
	}

	/**
	 * Says whether the enemy is hit at a specific index on the grid (passed as
	 * a parameter) or not.
	 * 
	 * @param hit
	 *            integer that represents the hit's index on the grid.
	 * @return true if the enemy is hit, false otherwise.
	 */
	public boolean isHit(int hit) {

		Boolean isHit = position.get(hit);

		// Enemy is not hit
		if (isHit == null)
			return false;
		else {
			// position.put(hit, true);
			position.remove(hit);
			if (position.isEmpty())
				sunk = true;
			return true;
		}
	}

	public boolean hasSunk() {
		return sunk;
	}
}
