// Modif JP
package netbattle.entities;

/**
 * 
 */

/**
 * @author Dominique
 *
 */
public class Game {

	Player player;
	Enemy enemy;
	int gridLength;

	public Game(int gridLength, Enemy enemy, Player player) {
		this.gridLength = gridLength;
		this.enemy = enemy;
		this.player = player;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @param player
	 *            the player to set
	 */
	public void setPlayer(Player player) {
		this.player = player;
	}

	/**
	 * @return the enemy
	 */
	public Enemy getEnemy() {
		return enemy;
	}

	/**
	 * @param enemy
	 *            the enemy to set
	 */
	public void setEnemy(Enemy enemy) {
		this.enemy = enemy;
	}

	/**
	 * @return the gridLength
	 */
	public int getGridLength() {
		return gridLength;
	}

	/**
	 * @param gridLength
	 *            the gridLength to set
	 */
	public void setGridLength(int gridLength) {
		this.gridLength = gridLength;
	}

	
	/**
	 * The player hit a battle
	 */
	public void play() {
		int hit;
		while (enemy.hasSunk() == false) {
			hit = player.hit();
			if (enemy.isHit(hit)) {
				System.out.println("Touché !");
			} else {
				System.out.println("Raté !");
			}
		}
	}

}
