package netbattle;
import netbattle.entities.Enemy;
import netbattle.entities.Game;
import netbattle.entities.Player;
import netbattle.ui.MainFrame;

/**
 * 
 */

/**
 * @author Dominique
 * 
 * Class used to load application.
 *
 */
public class Main {

	public static void main(String[] args) {
		
		// Grid length
		int gridLength = 7;
		// Enemy
		Enemy enemy = new Enemy();
		enemy.initPosition(3, gridLength);
		// Player
		Player player = new Player("Domi");
		// Starts a new game
		Game game = new Game(gridLength, enemy,player);
		
		// GUI
		MainFrame mainFrame = new MainFrame(game);
	}

}
