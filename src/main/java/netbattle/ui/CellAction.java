package netbattle.ui;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;

import netbattle.entities.Enemy;
import netbattle.entities.Game;

public class CellAction extends AbstractAction {

	private Game game;
	// This label is used to display info to the user
	private JLabel infoLabel;
	
	/**
	 * @param game Current game.
	 * @param infoLabel graphic label holding information about current hit.
	 */
	public CellAction(Game game, JLabel infoLabel) {
		this.game = game;
		this.infoLabel = infoLabel;
	}

	public void actionPerformed(ActionEvent e) {
		Cell cell = (Cell) e.getSource();
		int index = cell.getIndex();
		Enemy enemy = game.getEnemy();
		
		// Enemy is hit
		if (enemy.isHit(index)) {
			cell.setBackground(Color.RED);
			// Enemy has sunk
			if (enemy.hasSunk()) {
				infoLabel.setText("Touché coulé !");
			// Enemy is hit but has not sunk
			} else infoLabel.setText("Touché !");
		}
		// Enemy is not hit
		else {
			infoLabel.setText("Raté...");
		}
	}

}
