package netbattle.ui;

import javax.swing.JButton;

public class Cell extends JButton {

	private int index;
	
	public Cell(String text, int index) {
		super(text);
		this.setIndex(index);
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
