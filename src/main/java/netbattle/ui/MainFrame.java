package netbattle.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JButton;

import netbattle.entities.Game;

import java.awt.Color;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.Font;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private Game game;

	/**
	 * Launch the application.
	 */
	/**
	 * @param args
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { MainFrame frame = new MainFrame();
	 * 
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } }
	 * }); }
	 */

	/**
	 * @param game Current game.
	 */
	public MainFrame(Game game) {
		
		//TODO Fix this!
		super();

		// Game
		this.game = game;

		// Shut down application if the user clicks on the red cross
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(384, 254);
		// Center frame in screen
		setLocationRelativeTo(null);
		
		getContentPane().setLayout(null);

		JLabel lblTextIntro = new JLabel(
				"Cliquez sur les cases et essayez de couler le bateau ennemi !");
		lblTextIntro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTextIntro.setBounds(10, 49, 337, 14);
		getContentPane().add(lblTextIntro);
		
		// Info label
		JLabel lblStatusInfo = new JLabel("");
		lblStatusInfo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblStatusInfo.setBounds(63, 148, 125, 14);
		getContentPane().add(lblStatusInfo);

		// Same action on any cell
		CellAction action = new CellAction(game, lblStatusInfo);

		Cell cell = new Cell((String) null, 0);
		cell.setBounds(10, 85, 33, 29);
		cell.setAction(action);
		getContentPane().add(cell);

		Cell cell_1 = new Cell((String) null, 1);
		cell_1.setBounds(53, 85, 33, 29);
		cell_1.setAction(action);
		getContentPane().add(cell_1);

		Cell cell_2 = new Cell((String) null, 2);
		cell_2.setBounds(96, 85, 33, 29);
		cell_2.setAction(action);
		getContentPane().add(cell_2);

		Cell cell_3 = new Cell((String) null, 3);
		cell_3.setBounds(139, 85, 33, 29);
		cell_3.setAction(action);
		getContentPane().add(cell_3);

		Cell cell_4 = new Cell((String) null, 4);
		cell_4.setBounds(182, 85, 33, 29);
		cell_4.setAction(action);
		getContentPane().add(cell_4);

		Cell cell_5 = new Cell((String) null, 5);
		cell_5.setBounds(225, 85, 33, 29);
		cell_5.setAction(action);
		getContentPane().add(cell_5);

		Cell cell_6 = new Cell((String) null, 6);
		cell_6.setBounds(268, 85, 33, 29);
		cell_6.setAction(action);
		getContentPane().add(cell_6);

		JLabel lblStatus = new JLabel("Statut : ");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblStatus.setBounds(10, 147, 65, 14);
		getContentPane().add(lblStatus);

		/*
		 * contentPane = new JPanel(); contentPane.setBorder(new EmptyBorder(5,
		 * 5, 5, 5)); contentPane.setLayout(new GridLayout(0, 7, 0, 0));
		 * 
		 * // Label to display info to user JLabel infoLabel = new
		 * JLabel("Vous pouvez commencer à jouer !"); // Same action on any cell
		 * CellAction action = new CellAction(game, infoLabel); // Grid length
		 * int gridLength = game.getGridLength();
		 * 
		 * // Build grid (add cells) for (int i = 0; i < gridLength; i++) { Cell
		 * cell = new Cell(String.valueOf(i), i); cell.setAction(action);
		 * contentPane.add(cell); }
		 * 
		 * // Add info label to pane contentPane.add(infoLabel);
		 * 
		 * setContentPane(contentPane);
		 */
		setVisible(true);
	}
}
