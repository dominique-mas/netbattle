package netbattle.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTestJunit {

	/**
	 * Test of hasSunk function of Ennemy
	 */
	@Test
	public void testEnnemy1() {
		int gridLength 		= 9;
		Enemy enemy 		= new Enemy();
		enemy.initPosition(3, gridLength);
		Player player		= new Player("Domi");
		Game game 			= new Game(gridLength, enemy, player);
		
		assertFalse(  "Ennemy non touché ", enemy.isHit( 0));
		assertFalse(  "Ennemy non coulé ", game.getEnemy().hasSunk());
		
	}
	
	@Test
	public void testEnnemy2() {
		int gridLength 		= 9;
		Enemy enemy 		= new Enemy();
		enemy.initPosition(0, gridLength);
		Player player		= new Player("Domi");
		Game game 			= new Game(gridLength, enemy, player);

		assertTrue(  "Ennemy  touché ", enemy.isHit( 0));
		assertFalse( "Ennemy  non coulé ", game.getEnemy().hasSunk());
		
	}

	@Test
	public void testEnnemy3() {
		int gridLength 		= 9;
		Enemy enemy 		= new Enemy();
		enemy.initPosition(0, gridLength);
		Player player		= new Player("Domi");
		Game game 			= new Game(gridLength, enemy, player);

		assertTrue( "Ennemy  touché ", enemy.isHit( 0));
		assertTrue( "Ennemy  touché ", enemy.isHit( 1));
		assertTrue( "Ennemy  touché ", enemy.isHit( 2));
		assertTrue( "Ennemy  coulé ", game.getEnemy().hasSunk());
		
	}
	

	/**
	 * Test of hit function of Ennemy class
	 */
	@Test
	public void testHit() {
		int gridLength 		= 9;
		Enemy enemy 		= new Enemy();
		enemy.initPosition(3, gridLength);
		Player player		= new Player("Domi");
		Game game 			= new Game(gridLength, enemy, player);

		assertTrue(  "Ennemy touché ", game.getEnemy().isHit( 3));
		
	}
	
	

}
