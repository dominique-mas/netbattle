package netbattle.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestPlayer {

	@Test
	public void testHit() {
		Player player = new Player("Dominique");
		int i = player.hit();
		
		assertEquals(0, i);
	}
}
