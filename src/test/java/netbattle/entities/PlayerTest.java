package netbattle.entities;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void testHit() {
		Player player = new Player("Dominique");
		int i = player.hit();
		
		assertEquals(0, i);
	}
}
